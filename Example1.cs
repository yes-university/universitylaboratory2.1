﻿namespace UniversityLaboratory2._1;

static class Example1
{
    static void Example1Main()
    {
        double p = 3.14159;
        double x = 2.5;
        double y = Math.Cos(p * x) / (1 + x * x);
        Console.WriteLine();
        Console.WriteLine($"x= {x} \t y={y}");
    }
}